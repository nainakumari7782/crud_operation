const express = require('express');
const app = express();
const {MongoClient} = require('mongodb');
const mongodb= require('mongodb')
require("dotenv").config()

let db;
app.use(express.json());
const uri = process.env.MONGO_URI ;
const PORT=8000||process.env.port;
const client = new MongoClient(uri)

async function start(){
     try{
         await client.connect();
         console.log("connection ok")
          db=client.db();
  
         app.listen(PORT,()=>{

             console.log(`app is listening at port ${PORT}....`);
         })
          }
    catch(e){
        console.log(e)
    }
}
start()

app.post('/create-data',async(req,res)=>{
    if(req.body.name===''||req.body.class===''||req.body.state===''|| req.body.name===undefined||req.body.class===undefined||req.body.state===undefined){
        res.json({msg:"name class and subject must be provided!",success:false})
        return;
    }
    await db.collection('student').insertOne({name:req.body.name,class:req.body.class,state:req.body.state},function(err,info){
   
         res.json(info)
    })
})
app.get('/', async (req, res)=> {
  // getting all the data
   await db.collection('student')
    .find()
    .toArray(function (err, items) {
      res.send(items)
    })
})

app.put('/update-data', async (req, res)=> {
  // updating a data by it's ID and new value
   await db.collection('student').updateOne(
    { _id: new mongodb.ObjectId(req.body.id) },
    // { $set: { text: req.body.text } },
    { $set: {name:req.body.name,class:req.body.class,state:req.body.state} },
    function (err) {
      res.send('Success updated!')
      if(err){

          console.log(err)
      }
    //    res.json({msg:"Success updated!",success:true})
    }
  )
})

app.delete('/delete-data',async (req, res)=> {
  // deleting a data by it's ID
 const result= await db.collection('student').deleteOne(
    { _id: new mongodb.ObjectId(req.body.id) }
 
     )
     console.log(result);
  
     if(result.deletedCount==0){
        res.send("cannot find document")
        return;
     }
        res.send("deleted successfully")
    
})

